import {todos} from './state';
import {listen} from './lib/events';
import {addTodo, toggleTodoState, filterTodo} from './actions';

// Adiciona foco ao campo 'todoInput'
function focus() {
	document.getElementById('todoInput').focus();
}
// Cria um novo Todo
function newTodo(event) {
	const todoInput = document.getElementById('todoInput');
	todos.dispatch(addTodo(todoInput.value));
	event.stopPropagation();
	focus();
}


export function registerEventHandlers() {
    // Atribuiu um listen que verifica o evento de "Enter"
	listen('keyup', '#todoInput', event => {
    	if(event.keyCode == 13){
    		newTodo(event);
    	}
    });
	// Antigo listen modificado para o newTodo
    listen('click', '#addTodo', event => {	
    	newTodo(event);
    });

    listen('click', '.js_toggle_todo', event => {
        const id = Number.parseInt(event.target.getAttribute('data-id'), 10);
        todos.dispatch(toggleTodoState(id));
    });

    listen('change', '[name="filterType"]', event => {
        const type = event.target.value;
        filterTodo(type);
    });
}


