import {all} from './db';


export function toggleTodoState(id) {
    return {
        type: 'TODO_TOGGLE_DONE',
        id
    };
}

export function addTodo(text) {
    return {
        type: 'ADD_TODO',
        text
    }
}

export function filterTodo(type) {
    switch (type) {
        case 'all':
            showAll()
            break;
        case 'open':
            showAll();
            justOpened();
            break
        case 'close':
            showAll();
            justClosed();
            break;
    }
}

export function showAll() {
    const elements = document.getElementsByClassName('todo__item');
    for(var i = 0; i < elements.length; i++){
        elements[i].style.display = "block";
    }
}

export function justClosed() {
    const elements = document.getElementsByClassName('todo__item--open');
    for(var i = 0; i < elements.length; i++){
        elements[i].style.display = "none";
    }

}

export function justOpened() {
    const elements = document.getElementsByClassName('todo__item--done');
    for(var i = 0; i < elements.length; i++){
        elements[i].style.display = "none";
    }

}
