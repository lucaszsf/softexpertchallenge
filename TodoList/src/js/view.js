import {isEnabled} from './lib/feature';


export function render(el, state) {
    const todoItems = state.todos.map(renderTodoItem).join('');
    el.innerHTML = renderApp(
        renderInput(),
        renderTodos(todoItems)
    );
}

function renderApp(input, todoList) {
    if(isEnabled('renderBottom')) {
        return renderAddTodoAtBottom(input, todoList);
    } else {
        return renderAddTodoAtTop(input, todoList);
    }
}

function renderAddTodoAtTop(input, todoList) {
    return `<img src="/home/lucas/Downloads/Desafio_SoftExpert/softexpertchallenge/TodoList/src/img/todolist_logo.png" align="middle">
            <div id="app">
                ${input}
                ${todoList}
                ${enbltest.enableFilter ? renderFilter() : ''}
            </div>`;
}

function renderAddTodoAtBottom(input, todoList) {
    return `<div class="text-title">
                <h1> Todo List </h1>
            </div>
            <div id="app">
                ${enbltest.enableFilter && isEnabled('filterTop')  ? renderFilter() : ''}
                    ${todoList}
                    ${input}
            </div>`;
}

function renderInput() {
    return `<div class="todo__input">
                <input type="text" id="todoInput" placeholder="Add..">
                    <button class="btn" id="addTodo">Add</button>
            </div>`;
}

function renderTodos(todoItems) {
    return `<ul class="todo">${todoItems}</ul>`;
}

function renderTodoItem(todo) {
    const todoClass = `todo__item todo__item--${todo.done ? 'done' : 'open'}`;
    return `<li class="${todoClass}">
                <input class="js_toggle_todo" type="checkbox" data-id="${todo.id}"${todo.done ? ' checked' : ''}>
                ${todo.text}
            </li>`;
}


function renderFilter() {
    return `<div class="filter">
                <label class="radio-inline">
                    <input type="radio" name="filterType" value="all" checked=true> Mostrar todos<br>
                </label>
                <label class="radio-inline">
                    <input type="radio" name="filterType" value="open"> Somente abertos<br>
                </label>
                <label class="radio-inline">
                    <input type="radio" name="filterType" value="close"> Somente fechados<br>
                </label>
            </div>`;
}

